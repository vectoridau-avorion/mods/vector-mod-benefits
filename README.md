# Vector's Avorion Benefits Module

Entice members to return to your server by providing them
with monetary and/or resource benefits when they log on
daily. Increase the benefits for longer streaks (if you
want!) to encourage long-term log-ins.


# Installation

```TODO```


# Configuration

All configuration for this module is contained in a configuration
file at:
   `data/config/vector_mod_benefits.lua`

The documentation below explains the purpose and usage of each of
the variables in the configuration file.


## `WELCOME_MESSAGE_ENABLED`

If set to true then a welcome message will be
delivered to users when they join the server for the first
time. This welcome message can optionally include money and
resources for the user (see the `BENEFITS` variable).


## `WELCOME_MESSAGE_SENDER`

Set the name of the sender to use when delivering the
welcome message to users when they join the server for
the first time.


## `WELCOME_MESSAGE_HEADER`

Set the name of the welcome message to be delivered to
users when they join the server for the first time.


## `WELCOME_MESSAGE_TEXT`

Set the text content of the welcome message to be delivered
to users when they join the server for the first time.


## `BENEFITS`

Set the benefits to be delivered by the benefits module.

This variable has two sub-keys:

* `WELCOME`
* `DAILY`

The `WELCOME` sub-key is used to specify the benefits that
should be granted to a player in the welcome message when
that player joins the server for the very first time.

The `DAILY` sub-key contains an array of benefits tables, where
each benefits table within corresponds to the number of days
that a player has logged in for consecutively. That is to say, on
the first login day, the player gets the benefits from the first
table. On the second day, they get the benefits from the second,
and so on.

If the number of days that a player logs in for exceeds the number
of entries in the `DAILY` benefits array, then the player will
receive the benefits from the *last* entry in the table.

Each benefits table (both the `WELCOME` table, and each table
in the `DAILY` array) can specify any or all of the following keys:

* `money`
* `iron`
* `titanium`
* `naonite`
* `trinium`
* `xanion`
* `ogonite`
* `avorion`


## Sample Configuration File

```lua
#!/usr/bin/env lua
local config = {

    WELCOME_MESSAGE_ENABLED = true,
    WELCOME_MESSAGE_SENDER = "The Admin",
    WELCOME_MESSAGE_HEADER = "Welcome to the server!",
    WELCOME_MESSAGE_TEXT = [[
    Welcome to the server. It's dangerous out there...
    here, take this with you!
    ]],

    BENEFITS = {
        WELCOME = {
            money = 100000,
            iron = 10000,
            titanium = 20000,
            naonite = 10000,
            trinium = 0,
            xanion = 0,
            ogonite = 0,
            avorion = 0,
        },
        DAILY = {
            {money = 20000},
            {money = 40000},
            {money = 80000},
            {money = 160000, trinium = 2000},
            {money = 320000, xanion = 2000},
            {money = 640000, xanion = 3000, ogonite = 1000},
            {money = 1280000, ogonite = 2000, avorion = 100},
        }
    }
}

return config
```