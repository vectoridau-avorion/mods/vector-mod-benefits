.PHONY: all dist clean

all: dist

dist:
	mkdir -p ./dist
	rm -rf ./dist/*
	cp -r ./modinfo.lua ./dist/modinfo.lua
	cp -r ./data/ ./dist/data
	cp ./thumb.png ./dist/thumb.png
	cp ./README.md ./dist/README.md

clean:
	rm -rf ./dist/*

