------
-- Configuration for Vector's Avorion Benefits Module.

------
-- The configuration table for Vector's Avorion Benefits Module.
local config = {}

------
-- A boolean to turn the welcome message on or off.
-- If set to true, then a welcome message will be delivered to users
-- when they join the server for the first time. This welcome message
-- can optionally include money and resources for the user (based on
-- the BENEFITS config below).
config.WELCOME_MESSAGE_ENABLED = true

------
-- Set the name of the sender to use when delivering the
-- welcome message to users when they join the server for
-- the first time.
config.WELCOME_MESSAGE_SENDER = "The Admin"

------
-- Set the name of the welcome message to be delivered to
-- users when they join the server for the first time.
config.WELCOME_MESSAGE_HEADER = "Welcome to the server!"

------
-- Set the text content of the welcome message to be delivered
-- to users when they join the server for the first time.
config.WELCOME_MESSAGE_TEXT = "Welcome to the server. It's dangerous out there... here, take this with you!"

------
-- Set the benefits to be delivered by the benefits module, including
-- benefits for joining the server ("welcome" benefits), as well as log
-- in streaks ("daily" benefits).
--
--  Each benefits table (`WELCOME`, and `DAILIES`) can include any of
--  the following keys:
--
--  - `money`
--  - `iron`
--  - `titanium`
--  - `naonite`
--  - `trinium`
--  - `xanion`
--  - `ogonite`
--  - `avorion`
config.BENEFITS = {}

------
-- Set the initial benefits delivered (by message) on
-- a user's very first log in.
config.BENEFITS.WELCOME = {
    money = 100000,
    iron = 10000,
    titanium = 20000,
    naonite = 10000,
    trinium = 0,
    xanion = 0,
    ogonite = 0,
    avorion = 0
}

------
-- Set the benefits to be delivered on a daily basis,
-- for each consecutive day a user logs in.
--
--  Add as many days as you want to the daily array. Mix it up!
config.BENEFITS.DAILY = {
    {money = 20000}, -- Day 0 (first login)
    {money = 40000}, -- Day 1 (next login)
    {money = 80000}, -- Day 2
    {money = 160000}, -- Day 3
    {money = 320000}, -- Day 4
    {money = 640000}, -- Day 5
    {money = 1280000} -- Day 6 and beyond (still delivered daily)
}

return config
