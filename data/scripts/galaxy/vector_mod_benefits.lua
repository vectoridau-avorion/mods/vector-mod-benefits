------
-- Server Startup Script for Vector's Avorion Benefits Module.
-- Register player creation and login callbacks in order to
-- initialize players, and attach the benefits module to player
-- entities.

package.path = package.path .. ";data/scripts/lib/?.lua"
include("stringutility")

------
-- Import the Benefits library
local Benefits = include("data/scripts/lib/vector_mod_benefits")

------
-- Player Log In Callback.
--
-- Attach the benefits Avorion module to the player on login.
-- @param playerIndex The index of the player who logged in.
-- @return nil
function playerLoginCallback(playerIndex)
    local player = Player(playerIndex)
    print("Invoking vector_mod_benefits player login callback for player ${index} (${name})"%_t % {index = player.index, name = player.name})
    player:addScriptOnce("data/scripts/player/vector_mod_benefits.lua")
end

------
-- Module Start Up Handler.
--
-- Register initialization callbacks.
function initialize()
    print("Intializing vector_mod_benefits"%_t)
    Server():registerCallback("onPlayerLogIn", "playerLoginCallback")
end
