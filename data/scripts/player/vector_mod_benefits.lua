------
-- Benefits Avorion Module Implementation.

package.path = package.path .. ";data/scripts/lib/?.lua"
include("stringutility")

------
-- Import the Benefits Core Library.
local Benefits = include("data/scripts/lib/vector_mod_benefits")

------
-- Module Namespace.
--
-- The following namespace comment is *required* for
-- avorion to recognise the namespace. Do not alter or remove it.
--
-- namespace VectorModBenefits
VectorModBenefits = {}

------
-- Initialize this module for the player that it is being attached to.
--
-- If the player has never logged in, then initialize the benefits
--   module for that player (including the welcome pack).
--   Otherwise, determine if the player has added another day to their
--   streak, or if their streak should be reset.
-- 
-- Finally, send benefits to the player based on their login streak.
-- @return nil
function VectorModBenefits.initialize()
    if onServer() then
        local player = Player()
        local lastLogIn = Benefits.getLastLogin(player)
        if lastLogIn == -1 then
            print("Welcoming and initializing benefits module for player ${player}."%_t % {player = player.name})
            Benefits.initPlayer(player)
            if Benefits.shouldSendWelcomeMessage(player) then
                Benefits.sendWelcomeMessage(player)
            end
        end
        Benefits.updatePlayer(player)
    end
end
