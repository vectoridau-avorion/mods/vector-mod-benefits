------
-- Benefits Core Implementation.

package.path = package.path .. ";data/scripts/lib/?.lua"
include("stringutility")

------
-- Benefits 'Class'.
local Benefits = {}

------
-- Module Configuration loaded from `data/config/vector_mod_benefits.lua`
Benefits.CONFIG = include("data/config/vector_mod_benefits")

------
-- Constant: Player Value lookup key for the player's last login timestamp.
Benefits.KEY_LAST_LOGIN = "vector_benefits__last_login"

------
-- Constant: Player Value lookup key for the player's current login streak.
Benefits.KEY_LOGIN_STREAK = "vector_benefits__login_streak"

------
-- Retrieve the last timestamp the the given player received benefits.
--
-- @param player The player to check.
-- @return a timestamp, or -1 if the player has never logged in.
function Benefits.getLastLogin(player)
    local last = player:getValue(Benefits.KEY_LAST_LOGIN)
    if last == nil then
        last = -1
    end
    return last
end

------
-- Set the last timestamp that the given player received benefits.
--
-- @param player The player to update.
-- @param value The timestamp to set.
-- @return The player.
function Benefits.setLastLogin(player, value)
    player:setValue(Benefits.KEY_LAST_LOGIN, value)
    return player
end

------
-- Get the number of consecutive days the given player has logged in for.
--
-- @param player The player to check.
-- @return the number of consecutive days the player has logged in,
--   or 0 if the player has no streak or the streak has reset.
function Benefits.getLoginStreak(player)
    local streak = player:getValue(Benefits.KEY_LOGIN_STREAK)
    if streak == nil then
        streak = 0
    end
    return streak
end

------
-- Set the number of consecutive days the given player has logged in for.
--
-- @param player The player to update.
-- @param value The number of days to set.
-- @return The player.
function Benefits.setLoginStreak(player, value)
    player:setValue(Benefits.KEY_LOGIN_STREAK, value)
    return player
end

------
-- Initialize the benefits module for the given player.
--
-- Set the initial values for module-related variables, and send an
-- initial welcome message explaining the module's purpose.
-- @param player The player to initialize.
-- @return The player.
function Benefits.initPlayer(player)
    print("Initializing player benefits for player ${player}"%_t % {player = player.name})
    Benefits.setLastLogin(player, 0)
    Benefits.setLoginStreak(player, 0)
    player:sendChatMessage(
        "Server",
        ChatMessageType.Normal,
        "Welcome! This server provides benefits for login streaks. :)"%_t
    )
    return player
end

------
-- Determine if a welcome message should be sent to the given player.
--
-- This currently only checks the `WELCOME_MESSAGE_ENABLED` module setting.
-- @param player The player to check.
-- @return true if the welcome message should be sent, otherwise false.
function Benefits.shouldSendWelcomeMessage(player)
    return Benefits.CONFIG.WELCOME_MESSAGE_ENABLED
end

------
-- Send a welcome message to the given player.
--
-- This function uses the `BENEFITS.DAILY` module setting to determine
-- what (if any) monetary and/or resource gift to include in the message.
-- @param player The player to send the message to.
-- @return The player.
function Benefits.sendWelcomeMessage(player)
    -- Gather config
    local benefits = Benefits.CONFIG.BENEFITS.WELCOME

    -- Create the message
    local mail = Mail()
    mail.sender = Benefits.CONFIG.WELCOME_MESSAGE_SENDER
    mail.header = Benefits.CONFIG.WELCOME_MESSAGE_HEADER
    mail.text = Benefits.CONFIG.WELCOME_MESSAGE_TEXT
    mail.money = benefits.money or 0
    mail:setResources(
        benefits.iron or 0,
        benefits.titanium or 0,
        benefits.naonite or 0,
        benefits.trinium or 0,
        benefits.xanion or 0,
        benefits.ogonite or 0,
        benefits.avorion or 0
    )

    -- Send the message to the player
    player:addMail(mail)

    -- Log the action
    print(
        "Mailed welcome benefits to player ${player} :: ${benefits}"%_t %
            {
                player = player.name,
                benefits = Benefits.formatBenefitsTable(benefits)
            }
    )

    return player
end

------
-- Grant the daily benefit amount to the given player, based on
-- the number of consecutive days the player has logged in for.
--
-- @param player The player to grant benefits to.
-- @param loginDays The number of days the player has logged in
--   for (set to 1 if this is a new streak, 2 for logging in
--   two days in a row, and so on).
-- @return The player.
function Benefits.grantDailyBenefits(player, loginDays)
    -- Gather config
    local benefits = Benefits.getBenefitsTableForDay(player, loginDays)
    local benefitsStr = Benefits.formatBenefitsTable(benefits)

    -- Log the action
    print(
        "Granting daily login streak benefits for day ${loginDays} to player ${player} :: ${benefits}"%_t %
            {
                loginDays = tostring(loginDays),
                player = player.name,
                benefits = benefitsStr
            }
    )

    -- Send the benefits (without displaying the resource update
    -- on the RHS of the screen).
    player:receiveWithoutNotify(
        "Daily login streak benefits for day ${loginDays}"%_t % {loginDays = tostring(loginDays)},
        benefits.money or 0,
        benefits.iron or 0,
        benefits.titanium or 0,
        benefits.naonite or 0,
        benefits.trinium or 0,
        benefits.xanion or 0,
        benefits.ogonite or 0,
        benefits.avorion or 0
    )

    player:sendChatMessage(
        "Server",
        ChatMessageType.Normal,
        "Welcome, ${player}! Your login streak is ${loginDays} days."%_t %
            {
                player = player.name,
                loginDays = loginDays
            }
    )

    player:sendChatMessage(
        "Server",
        ChatMessageType.Normal,
        "For joining us today, you have earned the following daily login reward: ${benefits}"%_t %
            {
                benefits = benefitsStr
            }
    )

    return player
end

------
-- Notify a player that they have already logged in at least once today
-- and that they should come back tomorrow to claim more benefits.
function Benefits.alreadyLoggedInToday(player, loginDays)
    player:sendChatMessage(
        "Server",
        ChatMessageType.Normal,
        "Welcome ${player}! You have already received your daily login reward for today."%_t %
        {
            player = player.name
        }
    )
end

------
-- Send a message to the player to let them know what they'll miss out
-- on if they don't log in tomorrow...
-- @param player The player
-- @param loginDays The number of days the player has logged in
--   for (set to 1 if this is a new streak, 2 for logging in
--   two days in a row, and so on). This does NOT need to be modified
--   for this function - this function automatically adds 1 to the
--   number of login days.
function Benefits.teaseTomorrowBenefits(player, loginDays)
    local benefitsTomorrow = Benefits.getBenefitsTableForDay(player, loginDays + 1)
    local benefitsTomorrowStr = Benefits.formatBenefitsTable(benefitsTomorrow)

    player:sendChatMessage(
        "Server",
        ChatMessageType.Normal,
        "If you log in again tomorrow, you will receive: ${benefits}"%_t %
            {
                benefits = benefitsTomorrowStr
            }
    )
end

------
-- Retrieve the table of benefits applicable to a given day.
--
-- The first day of log-in starts at 1, and increases for every
-- additional day of consecutive login. If the day number exceeds
-- the total number of daily benefits defined, then the last daily
-- benefit table in the list is returned.
-- @param dayNumber The day number to retrieve the benefits table for.
--   The very first login would be day 1, the next would be day 2,
--   and so on.
-- @return The table of monetary and resource benefits associated
--   with the given day number, or the last daily benefit in
--   the benefits table if the day number exceeds the total
--   number of entries in the daily benefits table.
function Benefits.getBenefitsTableForDay(player, dayNumber)
    local benefitDaysCount = #Benefits.CONFIG.BENEFITS.DAILY
    if dayNumber > benefitDaysCount then
        benefitDaysCount = dayNumber
    end
    return Benefits.CONFIG.BENEFITS.DAILY[dayNumber]
end

------
-- Update a player based on their last login and login streak
-- details.
--
-- If a player has started a new day of a streak (or
-- the streak has been reset) then grant the player the daily
-- benefits for the n-day login.
-- @param player The player to update.
-- @return The player.
function Benefits.updatePlayer(player)
    print("Checking for updates to player benefits for player ${player}"%_t % {player = player.name})
    local lastLogin = Benefits.getLastLogin(player)
    local loginStreak = Benefits.getLoginStreak(player)

    local grant = false

    now = os.time()
    local oneDay = 60 * 60 * 24
    local sixHours = 60 * 60 * 6

    -- Make the threshold for a day to be a bit less than a full day.
    -- This allows for people to log in at slightly different times on
    -- consecutive days, but still gain their benefits.
    local oneDayThreshold = (oneDay - sixHours)

    -- up to 1 day
    -- No change as the last login was within the 1 day threshold
    if now < (lastLogin + oneDayThreshold) then
        -- do nothing; keep oldest login in last 24 hours
        -- between 1 and 2 days
        -- Set the login stamp and increase the login streak
    elseif now >= (lastLogin + oneDayThreshold) and now < (lastLogin + 2 * oneDay) then
        -- beyond 2 days
        -- Set the login stamp and clear the login streak
        lastLogin = now
        loginStreak = loginStreak + 1
        Benefits.setLastLogin(player, lastLogin)
        Benefits.setLoginStreak(player, loginStreak)
        grant = true
    else
        lastLogin = now
        loginStreak = 0
        Benefits.setLastLogin(player, lastLogin)
        Benefits.setLoginStreak(player, loginStreak)
        grant = true
    end

    local loginDays = 1
    if loginStreak ~= nil then
        loginDays = loginStreak + 1
    end

    if grant then
        Benefits.grantDailyBenefits(player, loginDays)
    else
        Benefits.alreadyLoggedInToday(player, loginDays)
    end

    Benefits.teaseTomorrowBenefits(player, loginDays)

    return player
end

------
-- Format a benefits table into a string.
-- @param benefitsTable The table of benefits to format.
-- @return The benefits table formatted as a string
function Benefits.formatBenefitsTable(benefitsTable)
    return "CR=${money} IR=${iron} TI=${titanium} NA=${naonite} TR=${trinium} XA=${xanion} OG=${ogonite} AV=${avorion}"%_t %
        {
            money = benefitsTable.money or 0,
            iron = benefitsTable.iron or 0,
            titanium = benefitsTable.titanium or 0,
            naonite = benefitsTable.naonite or 0,
            trinium = benefitsTable.trinium or 0,
            xanion = benefitsTable.xanion or 0,
            ogonite = benefitsTable.ogonite or 0,
            avorion = benefitsTable.avorion or 0
        }
end

-- Export the Benefits Module.
return Benefits
